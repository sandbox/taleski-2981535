<?php
use Drupal\Core\Render\BubbleableMetadata;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Drupal\Core\Url;

/**
 * Implements hook_token_info().
 */
function webform_confirm_token_info()
{
  $types['webform_confirm'] = array(
    'name' => t('Webform Confirm'),
    'description' => t('Tokens related to Webform Confirm')
  );
  $tokens['email_hash'] = array(
    'name' => t('Email hash field'),
    'description' => t('Email hash field')
  );

  return array(
    'types' => $types,
    'tokens' => array(
      'webform_confirm' => $tokens
    )
  );
}


/**
 * Implements hook_tokens().
 */
function webform_confirm_tokens($type, $tokens, array $data, array $options, \Drupal\Core\Render\BubbleableMetadata $bubbleable_metadata)
{
  $replacements = array();
  if($type == 'webform_confirm') {
    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'email_hash':
          $base = \Drupal::request()->getSchemeAndHttpHost();
          $text = $base.'/webform/confirm/mail/'.randHash();
          $replacements[$original] = $text;
          break;
      }
    }
  }
  return $replacements;
}


function randHash($len=32)
{
  return substr(md5(openssl_random_pseudo_bytes(20)),-$len);
}
