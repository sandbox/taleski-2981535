<?php

namespace Drupal\webform_confirm\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * WebformConfirmController controller.
 */
class WebformConfirmController extends ControllerBase {
  //confirm email
  public function confirm($email_hash) {
    $db = \Drupal::service('webform_confirm.db');

    //get email from database and check if it's not expired or opened.
    $webform_emails = $db->getEmail($email_hash);

    if (!($webform_emails)) {
      drupal_set_message("Already confirmed", 'error');
      return new RedirectResponse("/");
    }

    //update status in database
    $db->update(['status' => 1],
      ['=' => "email_hash->" . $email_hash]
    );

    //get webform submission
    $webform_submission = \Drupal\webform\Entity\WebformSubmission::loadMultiple([$webform_emails->sid]);
    $webform_submission = reset($webform_submission);

    //get id of webform
    $id_webform = $webform_submission->getWebform()->get("id");
    //get config , load config for the webform and get active emails for sending.
    $config   = \Drupal::config('webform_confirm.settings');
    $settings = $config->get($id_webform);

    //foreach active email handler triger send message.
    foreach ($settings['send_handler'] as $email) {
      $message_handler = $webform_submission->getWebform()->getHandler($email);
      $message         = $message_handler->getMessage($webform_submission);
      $message_handler->sendMessage($webform_submission, $message);
    }
    //redirect
    $redirect = $settings['redirect'];
    if (empty($redirect)) {
      $redirect = "/";
    }
    return new RedirectResponse($redirect);
  }
}
