<?php

namespace Drupal\webform_confirm\Service;

use Drupal\Core\Database\Connection;

/**
 * Defines a storage handler class that handles the Webform Confirm.
 */
class WebformConfirmDbLogic {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $insert;
  protected $update;
  protected $select;
  protected $globaldb;
  protected $table;
  protected $delete;

  /**
   * Constructs a WebformConfirmDbLogic object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->table    = "webform_emails";
    $this->select   = $database->select($this->table);
    $this->update   = $database->update($this->table);
    $this->insert   = $database->insert($this->table);
    $this->delete   = $database->delete($this->table);
    $this->globaldb = $database;
  }

  public function setTable($table) {
    $this->table = $table;
    return $this;
  }

  /**
   * Get all records from table mypage.
   */
  public function getAll() {
    return $this->getById();
  }

  /**
   * Get records by id from table mypage.
   */
  public function getById($id = NULL) {
    $query = $this->select;
    $query->fields('webform_emails');
    if ($id) {
      $query->condition('id_webform_confirm', $id);
    }
    $result = $query->execute()->fetchAll();
    if ($count = count($result)) {
      if ($count == 1) {
        $result = reset($result);
      }
      return $result;
    }
    return FALSE;
  }

  public function update(array $fields, array $conditions = []) {
    $query = $this->update;
    $query->fields($fields);
    if ($conditions) {
      foreach ($conditions as $key => $condition) {
        $key       = preg_replace('/\d+/', '', $key);
        $condition = explode("->", $condition);
        $query->condition($condition[0], $condition[1], $key);
      }
    }
    if ($query->execute()) {
      return TRUE;
    }

    return FALSE;
  }

  public function delete($conditions) {
    $query = $this->delete;

    foreach ($conditions as $key => $condition) {
      $key       = preg_replace('/\d+/', '', $key);
      $condition = explode("->", $condition);
      $query->condition($condition[0], $condition[1], $key);
    }
    if ($query->execute()) {
      return TRUE;
    }

    return FALSE;
  }

  public function getEmail($email_hash) {
    $query = $this->select;
    $query->fields("webform_emails");
    $query->condition('email_hash', $email_hash)
          ->condition("status", "0")
          ->condition("expired", 0);

    if ($result = $query->execute()->fetch()) {
      return $result;
    }
    return FALSE;
  }

  public function insert($fields) {
    $query = $this->insert;
    $query->fields($fields);
    if ($query->execute()) {
      return TRUE;
    }
    return FALSE;
  }

}
