Description:
------------
webform_confirm is a simple addon module for the webform module. You can define webform emails that are only sent when the user klicked on a link that he/she got in an other webform email. You can use this module to implement a double opt-in.

Dependencies:
------------
Webform

Installation:
------------
This module is installed like any other drupal module. 

How it works
------------
Normally, when you define an email in webform this email will be send immediately when the user clicked the webform submit button.

This module allows you to let webform send some of the emails and keep back others that will be sent later once the email address was confirmed.
The confirmation is done by the email(s) that are sent immediately after submission. They contain a link where the user can click on. When the user clicks the link from his/her email the webform email that was previously kept back will now be send.


Configuration:
------------
At first you will need to go to admin/structure/webform/manage/%/handlers and add email , and you must include confirm token ([webform_confirm:email_hash]) in message body.Confirm token is a confirmation link.Then add another email that will be sent after user click on the confirmation link. 

Then go to Webform Settings and open Confirmation tab or go to this url "admin/structure/webform/manage/%/settings/confirmation" where % is the webform id.At the end of confirmation settings form there is tab for confirmation email settings.

Simply click on "Use confirmation email feature" and it will activate this feature for this webform.
Then under "Select Confirm email" select confirmation mail with token that you've created and under "Select Emails to be send after confirmation." select at least one email to be sent after user click on confirmation link.

Also you need to specify where should user be redirected after he clicks on confirmation links.

There is settings to specify lifetime of confirmation request, default is 7 days, that means that after 7 days that link don't work anymore.If you check to delete internal data , it will delete requests from database table.

NOTE : Any emails used by this module will be set to DISABLED, because this module will handle sending of that emails.

