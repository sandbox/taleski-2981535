<?php
function _get_active_emails($settings) {
  $return = [];
  foreach ($settings as $key => $setting) {
    if ($setting == 1) {
      $return[] = $key;
    }
  }
  return $return;
}

function _get_submission($uuid) {
  $manager    = \Drupal::entityTypeManager()
                       ->getStorage("webform_submission");
  $submission = $manager->loadByProperties(['uuid' => $uuid]);
  $submission = reset($submission);
  return $submission;
}

function _validate_webform_handler_form(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  if ($form_state->hasValue('send_handler')) {
    $id_webform   = $form['#webform_id'];
    $config       = \Drupal::service('config.factory')
                           ->getEditable('webform_confirm.settings');
    $send_handler = $form_state->getValue('send_handler');
    $settings     = $config->get($id_webform);

    if ($send_handler == 0) {
      if (isset($settings['send_handler'][$form['#webform_handler_id']])) {
        if (count($settings['send_handler']) > 1) {
          unset($settings['send_handler'][$form['#webform_handler_id']]);
        }
        else {
          $form_state->setErrorByName("send_handler", "webform_confirm mush have minimum one sending hendler.");
          return FALSE;
        }
      }
    }
    else {
      if (!isset($settings['send_handler'][$form['#webform_handler_id']])) {
        $settings['send_handler'][$form['#webform_handler_id']] = $form['#webform_handler_id'];
      }
    }

    $config->set($id_webform, $settings)->save();
  }
}

function _validate_webform_settings_confirmation_form(&$form, \Drupal\Core\Form\FormStateInterface $form_state) {
  if ($form_state->hasValue('confirm_state')) {
    //get values
    $confirm_state   = $form_state->getValue('confirm_state');
    $redirect        = $form_state->getValue('confirm_redirect');
    $send_handlers   = $form_state->getValue('send_handler');
    $confirm_handler = $form_state->getValue('confirm_handler');
    $logged_in       = $form_state->getValue('logged_in');

    //remove unchecked handlers
    foreach ($send_handlers as $key => $send_handler) {
      if ($send_handler === 0) {
        unset($send_handlers[$key]);
      }
    }
    //get config
    $config = \Drupal::service('config.factory')
                     ->getEditable('webform_confirm.settings');
    //get webform id
    $webform    = $form_state->getFormObject()->getEntity();
    $id_webform = $webform->get("id");
    //Confirm email mush have token.
    if (_email_dont_have_token($id_webform, $confirm_handler)) {
      $form_state->setErrorByName("confirmation_email", "Confirmation email must include token [webform_confirm:email_hash].");
      return FALSE;
    }

    //Disable Active Handlers if enabled , on disable send active handlers to enabled.
    $disableHandlers   = $send_handlers;
    $disableHandlers[] = $confirm_handler;
    foreach ($webform->getHandlers() as $handler) {
      if (in_array($handler->getHandlerId(), $disableHandlers) && $confirm_state == 1) {
        $handler->setStatus(FALSE);
      }
      else {
        $handler->setStatus(TRUE);
      }
    }
    //IF not active , reset settings for that webform
    if ($confirm_state == 0) {
      $send_handlers   = 0;
      $confirm_handler = 0;
      $redirect        = "/";
    }
    // save/update settings for webform
    $settings = array(
      'send_handler'              => $send_handlers,
      'confirm_handler'           => $confirm_handler,
      'active'                    => $confirm_state,
      'redirect'                  => $redirect,
      'logged_in'                 => $logged_in,
      'request_lifetime_settings' => [
        'active'  => $form_state->getValue('request_lifetime'),
        'days'    => $form_state->getValue('days'),
        'hours'   => $form_state->getValue('hours'),
        'minutes' => $form_state->getValue('minutes'),
        'delete'  => $form_state->getValue('delete'),
      ],
    );
    $config->set($id_webform, $settings)->save();
  }
}

function _email_dont_have_token($id_webform, $handler) {
  $config_webform = \Drupal::config('webform.webform.' . $id_webform);
  $handlers       = $config_webform->get("handlers");
  $handlerBody    = $handlers[$handler]['settings']['body'];
  if (strpos($handlerBody, "webform_confirm:email_hash")) {
    return FALSE;
  }
  return TRUE;
}
